package ${package};


@javax.persistence.Entity
public class ${EntityName} implements BaseEntity {

	${fields}

	${getter}
	
	${setter}
	
	
}