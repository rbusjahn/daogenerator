package org.rubus.daogenerator.databasereader;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.rubus.daogenerator.model.Column;
import org.rubus.daogenerator.model.Table;

public class DatabaseReaderTest extends TestCase {

	private Logger log = Logger.getLogger(getClass());

	@Test(expected = IllegalStateException.class)
	public void testReadDatabaseThrowErrorWhenNoWhitelist() {
		DatabaseReader cut = new DatabaseReaderH2();
		try {
			cut.readDatabase("jdbc:h2:", "", "");
			org.junit.Assert.fail();
		} catch (Exception e) {
		}
	}

	public void testReadColumn() {
		DatabaseReaderH2 cut = new DatabaseReaderH2();
		Table table = new Table("DemoTable");
		String token = "FK_USER INT NOT NULL";
		cut.readColumn(token, table);

		Column col = table.getColumns().iterator().next();
		Assert.assertEquals("FK_USER", col.getName());
		Assert.assertEquals("INT", col.getType());
		Assert.assertTrue(col.isMandatory());

	}

	public void testwhiteListToCommaSeparatedString() {
		DatabaseReaderH2 cut = new DatabaseReaderH2();
		cut.addWhitelistEntry("A");
		cut.addWhitelistEntry("B");
		Assert.assertEquals(" 'A', 'B'", cut.whiteListToCommaSeparatedString());
	}

	public void testReadDatabase() {
		DatabaseReaderH2 cut = new DatabaseReaderH2();

		// SELECT TABLE_NAME, SQL FROM INFORMATION_SCHEMA.TABLES where table_NAME = 'TASKLINK';

		cut.addWhitelistEntry("JOB");
		cut.addWhitelistEntry("TASK");
		cut.addWhitelistEntry("USER");
		cut.readDatabase("jdbc:h2:../daogeneratorTestData/demodb;MVCC=TRUE", "", "");
	}

	public void testReadTableNameAndPrimaryKey() {
		String token = "CREATE CACHED TABLE PUBLIC.JOB(\n" +

		"	    ID INT DEFAULT (NEXT VALUE FOR PUBLIC.SYSTEM_SEQUENCE_71E8DABC_E126_4A60_A0A6_3B340C37919C) NOT NULL NULL_TO_DEFAULT SEQUENCE PUBLIC.SYSTEM_SEQUENCE_71E8DABC_E126_4A60_A0A6_3B340C37919C,\n";

		DatabaseReaderH2 cut = new DatabaseReaderH2();
		Table table = new Table();
		cut.readTableNameAndPrimaryKey(token, table);
		Assert.assertEquals("JOB", table.getName());
		Assert.assertEquals("ID", table.getPrivateKeyColumn().getName());
	}

	public void testReadTable() {
		String tableSql = "CREATE CACHED TABLE PUBLIC.JOB(\n" +

		"	    ID INT DEFAULT (NEXT VALUE FOR PUBLIC.SYSTEM_SEQUENCE_71E8DABC_E126_4A60_A0A6_3B340C37919C) NOT NULL NULL_TO_DEFAULT SEQUENCE PUBLIC.SYSTEM_SEQUENCE_71E8DABC_E126_4A60_A0A6_3B340C37919C,\n" +

		"	    VERSION INT,\n" +

		"	    ACTIVE INT,\n" +

		"	    FK_SUBENTITY INT NOT NULL,\n" +

		"	    TITLE VARCHAR(256) NOT NULL,\n" +

		"	    DESCRIPTION VARCHAR(2680),\n" +

		"	    PPC INT NOT NULL,\n" +

		"	    CREATIONTIME BIGINT NOT NULL,\n" +

		"	    STARTTIME BIGINT NOT NULL,\n" +

		"	    EXPIRETIME BIGINT NOT NULL,\n" +

		"	    STATUS VARCHAR(45) NOT NULL,\n" +

		"	    NUMBEROFCREDITS INT,\n" +

		"	    FKPARENTENTITY INT,\n" +

		"	)\n";

		DatabaseReaderH2 cut = new DatabaseReaderH2();
		Table table = cut.readTable(tableSql);
		Assert.assertNotNull(table);
		Assert.assertEquals("JOB", table.getName());
		Assert.assertTrue(columnFound("ID", table));
		Assert.assertTrue(columnFound("VERSION", table));

		Assert.assertTrue(columnFound("FK_SUBENTITY", table));

		Assert.assertTrue(columnFound("DESCRIPTION", table));

		Assert.assertTrue(columnFound("PPC", table));

	}

	protected boolean columnFound(String columnName, Table table) {
		boolean result = false;
		for (Column col : table.getColumns()) {
			if (col.getName().equalsIgnoreCase(columnName)) {
				result = true;
				break;
			}
		}
		return result;
	}
}
