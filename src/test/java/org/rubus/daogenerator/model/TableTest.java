package org.rubus.daogenerator.model;

import junit.framework.TestCase;

import org.junit.Assert;

public class TableTest extends TestCase {

	public void testGetPrivateKey() {
		Table cut = new Table("name");
		Column idColumn = new Column(cut, "ID", "int");
		idColumn.setPrivateKey(true);
		cut.addColumn(idColumn);
		cut.addColumn(new Column(cut, "aColumn", "varchar"));

		Column result = cut.getPrivateKeyColumn();
		Assert.assertNotNull(result);
		Assert.assertEquals(idColumn, result);

	}

	public void testAddMultiplePrivateKeyColumns() {
		Table cut = new Table("name");

		Column idColumn1 = new Column(cut, "ID", "int");
		idColumn1.setPrivateKey(true);
		cut.addColumn(idColumn1);

		Column idColumn2 = new Column(cut, "ID", "int");
		idColumn2.setPrivateKey(true);
		try {
			cut.addColumn(idColumn2);
			Assert.fail("multiple primary key could be added!");
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}
