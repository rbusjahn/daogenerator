package org.rubus.daogenerator.templates;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.rubus.daogenerator.model.Column;
import org.rubus.daogenerator.model.Table;

public class EntityModelTemplateTest extends TestCase {

	private Logger log = Logger.getLogger(getClass());

	@Test
	public void testCreate() {

		EntityModelTemplateWriter cut = new EntityModelTemplateWriter();
		cut.setTargetPackage("com.company");

		Table table = new Table();
		table.setName("User");
		Column idColumn = new Column(table, "id", "long");
		idColumn.setPrivateKey(true);
		table.addColumn(idColumn);
		table.addColumn(new Column(table, "name", "string"));
		table.addColumn(new Column(table, "email", "string"));

		String result = cut.createPojo(table);
		log.info("\n" + result);

		write(result, new File("User.java"));
	}

	private void write(String result, File file) {
		try {
			OutputStream fout = new FileOutputStream(file);
			log.debug("write file:" + file.getAbsolutePath());
			fout.write(result.getBytes("UTF-8"));
			fout.flush();
			fout.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
