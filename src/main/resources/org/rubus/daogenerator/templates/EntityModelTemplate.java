package ${package};

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@javax.persistence.Entity
public class ${EntityName} implements BaseEntity {

	${fields}

	${getter}
	
	${setter}
	
	
}