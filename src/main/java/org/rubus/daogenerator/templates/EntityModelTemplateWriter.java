package org.rubus.daogenerator.templates;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.log4j.Logger;
import org.rubus.daogenerator.model.Column;
import org.rubus.daogenerator.model.Table;

public class EntityModelTemplateWriter {

	private Logger log = Logger.getLogger(getClass());

	private String targetPackage = "demo.package";

	public String createPojo(Table table) {

		String template = getTemplate();

		String content = template.replace("${package}", targetPackage);

		content = content.replace("${EntityName}", table.getName());

		content = content.replace("${fields}", generateFields(table));

		content = content.replace("${getter}", generateGetter(table));

		content = content.replace("${setter}", generateSetter(table));

		return content;
	}

	protected String generateFields(Table table) {
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		for (Column col : table.getColumns()) {
			if (col.isPrivateKey()) {
				addLine("@javax.persistence.Id()", sb);
				addLine("@javax.persistence.GeneratedValue()", sb);
				addLine("private " + getDataType(col) + " " + getFieldName(col) + ";", sb);
			} else {
				addLine("@javax.persistence.Column()", sb);
				addLine("private " + getDataType(col) + " " + getFieldName(col) + ";", sb);
			}
			sb.append("\n");
		}
		log.info("fields:" + sb.toString());
		return sb.toString();
	}

	protected String generateGetter(Table table) {
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		for (Column col : table.getColumns()) {
			String name = col.getName().toLowerCase();
			name = "get" + name.substring(0, 1).toUpperCase() + name.substring(1);
			addLine("public " + getDataType(col) + " " + name + "() {", sb);
			addLine("	return this." + col.getName() + ";", sb);
			addLine("}", sb);
			sb.append("\n");
		}

		return sb.toString();
	}

	protected String generateSetter(Table table) {
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		for (Column col : table.getColumns()) {
			String name = col.getName().toLowerCase();
			name = "set" + name.substring(0, 1).toUpperCase() + name.substring(1);
			addLine("public void " + name + "(" + getDataType(col) + " value) {", sb);
			addLine("	this." + col.getName() + " = value;", sb);
			addLine("}", sb);
			sb.append("\n");
		}

		return sb.toString();
	}

	protected String getFieldName(Column col) {

		return col.getName().toLowerCase();
	}

	protected String getDataType(Column col) {
		String result = "undefined mapping:" + col.getType();
		if (col.getType().toLowerCase().equals("long")) {
			result = "java.lang.Long";
		} else if (col.getType().toLowerCase().equals("string")) {
			result = "java.lang.String";
		}
		return result;
	}

	private void addLine(String s, StringBuilder sb) {
		sb.append("\t" + s + "\n");
	}

	protected String getTemplate() {

		log.info("path:" + getClass().getResource(""));
		String filename = getClass().getResource("EntityModelTemplate.java").getFile();
		log.info("p:" + filename);

		String content = "";

		try {
			InputStream in = new FileInputStream(new File(filename));
			byte[] buffer = new byte[in.available()];
			in.read(buffer);
			content = new String(buffer, "UTF-8");
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return content;
	}

	public void setTargetPackage(String targetPackage) {
		this.targetPackage = targetPackage;
	}

}
