package org.rubus.daogenerator.processor;

import java.util.List;

import org.apache.log4j.Logger;
import org.rubus.daogenerator.databasereader.DatabaseReaderH2;
import org.rubus.daogenerator.model.Table;

public class GeneratorProcess {

	private Logger log = Logger.getLogger(getClass());

	public void start(String url, String username, String pwd, String[] tableNames) {

		DatabaseReaderH2 dbReader = new DatabaseReaderH2();

		for (String name : tableNames) {
			dbReader.addWhitelistEntry(name);
		}

		List<Table> tables = dbReader.readDatabase(url, username, pwd);

		for (Table tab : tables) {
			log.debug(tab);
		}
	}
}
