package org.rubus.daogenerator.databasereader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.rubus.daogenerator.model.Column;
import org.rubus.daogenerator.model.Table;
import org.rubus.daogenerator.model.TableReference;

public class DatabaseReaderH2 extends DatabaseReader {

	@Override
	protected List<Table> read(Connection conn) {
		List<Table> tables = new ArrayList<Table>();
		try {
			Statement stmt = conn.createStatement();
			String sqlQuery = getSql();
			log.debug("sql:" + sqlQuery);
			if (stmt.execute(sqlQuery)) {
				ResultSet rs = stmt.getResultSet();
				while (rs.next()) {
					int index = 1;
					String tableName = rs.getString(index++);
					String sqlData = rs.getString(index++);
					log.debug("tableName:" + tableName);
					log.debug("sqlData:" + sqlData);

					Table table = readTable(sqlData);
					tables.add(table);
				}
			}

			List<TableReference> tableRefList = readTableReferences(stmt);
			for (Table tab : tables) {
				String tabName = tab.getName();
				for (TableReference tabRef : tableRefList) {
					String tabFrom = tabRef.getTableFrom();
					if (tabName.equals(tabFrom)) {
						String fkColumnName = tabRef.getFkColumnName();
						String tableTo = tabRef.getFkTable();
						log.debug(tabFrom + "-" + fkColumnName + "-" + tableTo);
						tab.addTableReference(tabRef);
					}
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
		return tables;
	}

	protected List<TableReference> readTableReferences(Statement stmt) throws SQLException {
		List<TableReference> list = new ArrayList<TableReference>();
		String refSqlQuery = getTableRefSql();
		if (stmt.execute(refSqlQuery)) {
			ResultSet rs = stmt.getResultSet();
			while (rs.next()) {
				int index = 1;
				String tableName = rs.getString(index++);
				String refTableName = rs.getString(index++);
				String refColumnName = rs.getString(index++);
				list.add(new TableReference(refTableName, refColumnName, tableName));
			}
		}
		return list;
	}

	protected String getSql() {
		String whitelistData = whiteListToCommaSeparatedString();

		return "SELECT TABLE_NAME,SQL FROM INFORMATION_SCHEMA.TABLES where table_NAME IN (" + whitelistData + ");";

	}

	protected String getTableRefSql() {
		return "SELECT PKTABLE_NAME , FKTABLE_NAME , FKCOLUMN_NAME  FROM INFORMATION_SCHEMA.CROSS_REFERENCES ";
	}

	protected String whiteListToCommaSeparatedString() {
		StringBuilder sb = new StringBuilder();
		int index = 0;
		int size = whitelist.size();
		for (String s : whitelist) {
			sb.append(" '");
			sb.append(s);
			sb.append("'");
			if (index < (size - 1)) {
				sb.append(",");
			}
			index++;
		}
		return sb.toString();
	}

	@Override
	protected Connection connectToH2Db(String url, String username, String pwd) {
		try {
			Class.forName("org.h2.Driver");
			Connection conn = DriverManager.getConnection(url, username, pwd);
			return conn;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public Table readTable(String tableSql) {
		Table table = new Table();
		final String COMMA = ",";

		String tokens1[] = tableSql.split(COMMA);
		for (String token : tokens1) {

			token = token.trim().toUpperCase();
			log.debug("token:" + token);

			readTableNameAndPrimaryKey(token, table);
			readColumn(token, table);

		}
		return table;
	}

	protected void readTableNameAndPrimaryKey(String token, Table table) {
		if (token.contains(" TABLE ")) {
			if (token.contains("CREATE ")) {
				String temp = token.substring(token.indexOf("TABLE") + "TABLE".length());
				log.debug("temp:" + temp);
				int indexOfDot = temp.indexOf(".");
				if (indexOfDot > 0) {
					temp = temp.substring(indexOfDot + 1);
					log.debug("temp:" + temp);
				}
				int indexOfBracket = temp.indexOf("(");
				if (indexOfBracket > 0) {
					String name = temp.substring(0, indexOfBracket);
					table.setName(name);
				}
				log.debug("temp:" + temp);
				temp = temp.substring(indexOfBracket + 1).trim();
				log.debug("temp:" + temp);
				String[] chunks = temp.split(" ");
				if (chunks.length > 0) {
					String columnName = chunks[0];
					String columnType = chunks[1];
					Column idColumn = new Column(table, columnName, columnType);
					idColumn.setPrivateKey(true);
					table.addColumn(idColumn);
				}
			}
		}
	}

	protected void readColumn(String token, Table table) {
		String[] chunks = token.split(" ");
		boolean required = token.toUpperCase().contains("NOT NULL");
		log.debug(token);
		if (chunks.length > 1) {
			String columnName = chunks[0];
			String columnType = chunks[1];
			Column column = new Column(table, columnName, columnType);
			column.setMandatory(required);
			table.addColumn(column);
		}
	}

}
