package org.rubus.daogenerator.databasereader;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.rubus.daogenerator.model.Table;

public abstract class DatabaseReader {

	protected Logger log = Logger.getLogger(getClass());

	protected Set<String> whitelist = new HashSet<String>();

	public List<Table> readDatabase(String url, String username, String pwd) {
		List<Table> tables = new ArrayList<Table>();
		if (whitelist.isEmpty()) {
			throw new IllegalStateException("whitelist cannot be empty.");
		}
		Connection conn = connectToH2Db(url, username, pwd);
		try {

			tables = read(conn);

		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			close(conn);
		}
		return tables;
	}

	public void addWhitelistEntry(String tableName) {
		this.whitelist.add(tableName);
	}

	protected void close(Connection conn) {
		try {
			conn.close();
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	protected abstract Connection connectToH2Db(String url, String username, String pwd);

	protected abstract List<Table> read(Connection conn);

}
