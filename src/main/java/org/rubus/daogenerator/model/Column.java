package org.rubus.daogenerator.model;

public class Column {

	private boolean privateKey;
	private Table table;
	private String name;
	private String type;
	private int length;
	private int precision;
	private boolean mandatory;

	public final static Column NO_COLUMN = new Column(null, "NO_COLUMN", "");

	public Column(Table itsTable, String name, String type) {
		this.table = itsTable;
		this.name = name;
		this.type = type;
	}

	public boolean isPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(boolean privateKey) {
		this.privateKey = privateKey;
	}

	public Table getTable() {
		return table;
	}

	public void setTable(Table table) {
		this.table = table;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getPrecision() {
		return precision;
	}

	public void setPrecision(int precision) {
		this.precision = precision;
	}

	public void setMandatory(boolean b) {
		this.mandatory = b;
	}

	public boolean isMandatory() {
		return this.mandatory;
	}
}