package org.rubus.daogenerator.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Table {

	private String name;
	private Set<Column> columns = new HashSet<Column>();
	private Set<TableReference> references = new HashSet<TableReference>();

	public Table() {

	}

	public Table(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addColumn(Column c) {
		if (hasPrivateKey()) {
			if (c.isPrivateKey()) {
				throw new IllegalStateException("there is already a private key");
			}
		}
		columns.add(c);
	}

	protected boolean hasPrivateKey() {
		return !getPrivateKeyColumn().equals(Column.NO_COLUMN);
	}

	public Set<Column> getColumns() {
		return Collections.unmodifiableSet(columns);
	}

	public void addTableReference(TableReference tr) {
		this.references.add(tr);
	}

	public Set<TableReference> getTableReferences() {
		return Collections.unmodifiableSet(references);
	}

	public Column getPrivateKeyColumn() {
		Column result = Column.NO_COLUMN;
		for (Column col : columns) {
			if (col.isPrivateKey()) {
				result = col;
				break;
			}
		}
		return result;
	}

	@Override
	public String toString() {
		ToStringBuilder tsb = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);
		tsb.append("name", this.name);
		for (Column col : getColumns()) {
			tsb.append("column:", col.getName());
		}
		for (TableReference tabRef : getTableReferences()) {
			tsb.append("ref:", tabRef.getFkColumnName() + "->" + tabRef.getFkTable());
		}
		return tsb.build();
	}
}
