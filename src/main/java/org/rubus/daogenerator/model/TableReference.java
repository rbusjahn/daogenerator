package org.rubus.daogenerator.model;

public class TableReference {

	private String tableFrom;
	private String fkfkColumnName;
	private String fkTable;

	public TableReference(String tableFrom, String fkColumnName, String fkTable) {
		this.tableFrom = tableFrom;
		this.fkfkColumnName = fkColumnName;
		this.fkTable = fkTable;
	}

	public String getTableFrom() {
		return tableFrom;
	}

	public String getFkTable() {
		return fkTable;
	}

	public String getFkColumnName() {
		return fkfkColumnName;
	}

}
