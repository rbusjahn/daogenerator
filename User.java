package com.company;


@javax.persistence.Entity
public class User implements BaseEntity {

	
	@javax.persistence.Column()
	private java.lang.String name;

	@javax.persistence.Column()
	private java.lang.String email;

	@javax.persistence.Id()
	@javax.persistence.GeneratedValue()
	private java.lang.Long id;



	
	public java.lang.String getName() {
		return this.name;
	}

	public java.lang.String getEmail() {
		return this.email;
	}

	public java.lang.Long getId() {
		return this.id;
	}


	
	
	public void setName(java.lang.String value) {
		this.name = value;
	}

	public void setEmail(java.lang.String value) {
		this.email = value;
	}

	public void setId(java.lang.Long value) {
		this.id = value;
	}


	
	
}